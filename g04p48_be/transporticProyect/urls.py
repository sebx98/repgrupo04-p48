from django.urls import path
from rest_framework_simplejwt.views import (
    TokenObtainPairView, TokenRefreshView)
from transporticApp import views

urlpatterns = [
    # user
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('userList/', views.UserListView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
    path('user/update/<int:pk>/', views.UserUpdateView.as_view()),
    path('user/delete/<int:pk>/', views.UserDeleteView.as_view()),

    # busdriver
    path('busdriver/', views.BusDriverCreateView.as_view()),
    path('busDriverList/', views.BusDriverListView.as_view()),
    path('busDriver/<int:pk>/', views.BusDriverDetailView.as_view()),
    path('busDriver/update/<int:pk>/', views.BusDriverUpdate.as_view()),
    path('busDriver/delete/<int:pk>/', views.BusDriverDelete.as_view()),
    # Bus
    path('bus/', views.BusCreateView.as_view()),
    path('busList/', views.BusListView.as_view()),
    path('bus/<int:pk>/', views.BusDetailView.as_view()),
    path('bus/update/<int:pk>/', views.BusUpdate.as_view()),
    path('bus/delete/<int:pk>/', views.BusDelete.as_view()),
    # Points
    path('point/<int:pk>/', views.PointDetailView.as_view()),
    path('point/update/<int:pk>/', views.PointUpdate.as_view()),
    path('point/delete/<int:pk>/', views.PointDelete.as_view()),
    path('point/list/', views.PointListView.as_view()),

    #


]
