from django.db import models
from .routeBus import RouteBus
from .user import User


class Travel(models.Model):
    id = models.BigAutoField(primary_key=True)
    activeTravel = models.BooleanField(default=True)
    price = models.IntegerField(default=0)
    points = models.IntegerField(default=0)
    client = models.ForeignKey(
        User, related_name='travelClient', on_delete=models.CASCADE)
    routeBus = models.ForeignKey(
        RouteBus, related_name='travelRoute', on_delete=models.CASCADE)
