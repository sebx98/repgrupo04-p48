from django.db import models
class BusDriver(models.Model):
 id = models.BigAutoField(primary_key=True)
 documentType=models.CharField('documentType',max_length=10)
 document=models.IntegerField('Document',max_length=45)
 name = models.CharField('Name', max_length = 45)
 address=models.CharField('Address',max_length=100)
 phone=models.IntegerField('Phone',max_length=30)
 email = models.EmailField('Email', max_length = 100)
 username = models.CharField('Username', max_length = 15, unique=True)
 password = models.CharField('Password', max_length = 256)
 