from django.db import models
from .busDriver import BusDriver
class Bus(models.Model):
 id = models.AutoField(primary_key=True)
 busDriver=models.ForeignKey(BusDriver, related_name='bus', on_delete=models.CASCADE)
 plate = models.CharField('Plate',max_length=20)
 seat = models.IntegerField(default=0)
 model = models.CharField('Model',max_length=45)
 aviable = models.BooleanField(default=True)
