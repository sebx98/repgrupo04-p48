from django.db import models
from .user import User


class Point(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='point', on_delete=models.CASCADE)
    amount = models.IntegerField(default=0)
    lastChangeDate = models.DateTimeField()
    isActive = models.BooleanField(default=True)
