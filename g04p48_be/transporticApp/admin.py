from django.contrib import admin

from transporticApp.models.point import Point
from .models.user import User
from .models.busDriver import BusDriver
from .models.bus import Bus
from .models.routeBus import RouteBus
from .models.travel import Travel
admin.site.register(User)
admin.site.register(Point)
admin.site.register(BusDriver)
admin.site.register(Bus)
admin.site.register(RouteBus)
admin.site.register(Travel)
