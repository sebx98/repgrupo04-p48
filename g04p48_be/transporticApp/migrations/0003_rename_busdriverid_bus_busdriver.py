# Generated by Django 3.2.8 on 2021-10-29 21:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('transporticApp', '0002_alter_bus_busdriverid'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bus',
            old_name='busDriverId',
            new_name='busDriver',
        ),
    ]
