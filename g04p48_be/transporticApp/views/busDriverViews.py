# framework
from rest_framework.response import Response
from rest_framework import status, views, generics

from django.conf import settings

# models

from transporticApp.models.busDriver import BusDriver
# serializers
from transporticApp.serializers.busDriverSerializer import BusDriverSerializer


class BusDriverCreateView(views.APIView):
    def post(self, request, *args, **kwargs):
        serializer = BusDriverSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(status=status.HTTP_201_CREATED)


class BusDriverListView(generics.ListCreateAPIView):
    queryset = BusDriver.objects.all()
    serializer_class = BusDriverSerializer

    def list(self, request):

        queryset = self.get_queryset()
        serializer = BusDriverSerializer(queryset, many=True)
        return Response(serializer.data)


class BusDriverDetailView(generics.RetrieveAPIView):
    serializer_class = BusDriverSerializer
    queryset = BusDriver.objects.all()

    def get_queryset(self, *args, **kwargs):
        queryset = BusDriver.objects.filter(id=self.kwargs['pk'])
        return queryset


class BusDriverUpdate(generics.UpdateAPIView):
    serializer_class = BusDriverSerializer
    queryset = BusDriver.objects.all()

    def put(self, request, *args, **kwargs):
        queryset = BusDriver.objects.filter(id=self.kwargs['pk'])
        return super().update(request, *args, **kwargs)


class BusDriverDelete(generics.DestroyAPIView):
    serializer_class = BusDriverSerializer
    queryset = BusDriver.objects.all()

    def delete(self, request, *args, **kwargs):

        return super().destroy(request, *args, **kwargs)
