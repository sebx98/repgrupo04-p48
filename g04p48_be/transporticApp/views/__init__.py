from .userViews import UserCreateView, UserDetailView, UserUpdateView, UserDeleteView,UserListView
from .busDriverViews import BusDriverCreateView,BusDriverListView,BusDriverDelete,BusDriverUpdate,BusDriverDetailView
from .pointViews import PointDetailView,PointUpdate,PointDelete,PointListView
from .busViews import BusCreateView,BusListView,BusDetailView,BusDelete,BusUpdate
