# framework
from rest_framework.response import Response
from rest_framework import status, views, generics

from django.conf import settings

# models
from transporticApp.models.bus import Bus

# serializers
from transporticApp.serializers.busSerializer import BusSerializer


class BusCreateView(views.APIView):
    def post(self, request, *args, **kwargs):
        serializer = BusSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(status=status.HTTP_201_CREATED)


class BusListView(generics.ListCreateAPIView):
    queryset = Bus.objects.all()
    serializer_class = BusSerializer

    def list(self, request):

        queryset = self.get_queryset()
        serializer = BusSerializer(queryset, many=True)
        return Response(serializer.data)


class BusDetailView(generics.RetrieveAPIView):
    serializer_class = BusSerializer
    queryset = Bus.objects.all()

    def get_queryset(self, *args, **kwargs):
        queryset = Bus.objects.filter(id=self.kwargs['pk'])
        return queryset


class BusUpdate(generics.UpdateAPIView):
    serializer_class = BusSerializer
    queryset = Bus.objects.all()

    def put(self, request, *args, **kwargs):
        queryset = Bus.objects.filter(id=self.kwargs['pk'])
        return super().update(request, *args, **kwargs)


class BusDelete(generics.DestroyAPIView):
    serializer_class = BusSerializer
    queryset = Bus.objects.all()

    def delete(self, request, *args, **kwargs):

        return super().destroy(request, *args, **kwargs)
