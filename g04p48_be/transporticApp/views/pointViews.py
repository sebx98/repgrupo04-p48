from django.db.models import query
from rest_framework import serializers, status, views
from rest_framework.response import Response
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from django.conf import settings
from rest_framework import generics, status
from transporticApp.models.point import Point
from rest_framework.permissions import IsAuthenticated

from transporticApp.serializers.pointSerializer import PointSerializer



class PointDetailView(generics.RetrieveAPIView):
    serializer_class=PointSerializer
    queryset = Point.objects.all()
    def get_queryset(self,*args, **kwargs):
        queryset = Point.objects.filter(id=self.kwargs['pk'])
        return queryset
        
class PointListView(generics.ListCreateAPIView):
    queryset = Point.objects.all()
    serializer_class = PointSerializer

    def list(self, request):
        # Note the use of `get_queryset()` instead of `self.queryset`
        queryset = self.get_queryset()
        serializer = PointSerializer(queryset, many=True)
        return Response(serializer.data)

class PointUpdate(generics.UpdateAPIView):
    serializer_class=PointSerializer
    queryset = Point.objects.all()
    def put(self,request,*args, **kwargs):
        queryset = Point.objects.filter(id=self.kwargs['pk'])
        return super().update(request, *args, **kwargs) 

class PointDelete(generics.DestroyAPIView):
    serializer_class=PointSerializer
    queryset = Point.objects.all()
    def delete(self,request,*args, **kwargs):
        #queryset = Point.objects.filter(id=self.kwargs['pk'])
        return super().destroy(request, *args, **kwargs)        