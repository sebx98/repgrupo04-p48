from django.apps import AppConfig


class TransporticappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'transporticApp'
