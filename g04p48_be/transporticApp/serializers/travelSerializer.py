from transporticApp.models.travel import Travel
from rest_framework import serializers
class TravelSerializer(serializers.ModelSerializer):
 class Meta:
  model = Travel 
  fields = ['activeTravel','price','points']