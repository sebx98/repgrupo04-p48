from rest_framework import serializers
from transporticApp.models.user import User
from transporticApp.models.point import Point
from transporticApp.serializers.pointSerializer import PointSerializer


class UserSerializer(serializers.ModelSerializer):
    point = PointSerializer()

    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'documentType',
                  'document', 'name', 'address', 'phone', 'email', 'point']

    def create(self, validated_data):
        pointData = validated_data.pop('point')
        userInstance = User.objects.create(**validated_data)
        Point.objects.create(user=userInstance, **pointData)
        return userInstance
       

    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        point = Point.objects.get(user=obj.id)
        return {
            'id': user.id,
            'username': user.username,
            'name': user.name,
            'email': user.email,
            'point': {
                'id': point.id,
                'amount': point.amount,
                'lastChangeDate': point.lastChangeDate,
                'isActive': point.isActive
            }
        }
