from rest_framework import serializers
from transporticApp.models.user import User
from transporticApp.models.point import Point
from transporticApp.serializers.pointSerializer import PointSerializer


class UserSpecialSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','documentType','document','name','address','phone','email','username','password']

