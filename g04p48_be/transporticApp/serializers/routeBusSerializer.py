from transporticApp.models.routeBus import RouteBus
from rest_framework import serializers
class RouteBusSerializer(serializers.ModelSerializer):
 class Meta:
  model = RouteBus 
  fields = ['depatureDate','arrivalDate','originCity','destinyCity']