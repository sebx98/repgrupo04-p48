from rest_framework import serializers
from transporticApp.models.busDriver import BusDriver
from transporticApp.models.bus import Bus
from transporticApp.serializers.busSerializer import BusSerializer


class BusDriverSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = BusDriver
        fields = ['id','documentType', 'document', 'name',
                  'address', 'phone', 'email', 'username', 'password']

