from transporticApp.models.bus import Bus
from rest_framework import serializers

class BusSerializer(serializers.ModelSerializer):
 class Meta:
  model = Bus 
  fields = ['plate','seat','model','aviable','busDriver']
