import { createRouter, createWebHistory } from "vue-router";


const routes = [{
    
      path: '/',
      name: "inicio",
      component: () => import(/* webpackChunkName: "inicio" */ "./components/Inicio.vue"),
    },    
    {
      path: '/user/login',
      name: "login",
      component: () =>
        import(/* webpackChunkName: "login" */ "./components/Login.vue"),
    },
    {
      path: '/user/signUp',
      name: "signUp",
      component: () =>
        import(/* webpackChunkName: "register" */ "./components/SignUp.vue"),
    },
    {
      path: '/user/profile',
      name: "profile",
      component: () => 
        import(/* webpackChunkName: "profile" */ "./components/PerfilUser.vue"),
    },
    {
      path: '/routes',
      name: "routes",
      component: () =>
        import(/* webpackChunkName: "rutas" */ "./components/Rutas/Rutas.vue"),
    },    
    {
      path: '/bus',
      name: "bus",
      component: () =>
        import(/* webpackChunkName: "conductor" */ "./components/Bus/Bus.vue"),
    },
    {
      path: '/driver',
      name: "driver",
      component: () =>
        import(/* webpackChunkName: "viajes" */ "./components/Driver/Driver.vue"),
    }
  
  

];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

  export default router;
